class Enemy
{
constructor()
{
	const pos = new Vec2( 600,400 );
	const size = 32;
	const vel = new Vec2( 0.0,0.0 );
	const decayRate = 0.95;
	// 
	this.Update=()=>
	{
		pos.Add( vel );
		vel.Multiply( decayRate );
	}
	
	this.Draw=( gfx )=>
	{
		gfx.DrawRect( pos,new Vec2( size,size ),"red" );
	}
	
	this.Push=( amount )=>
	{
		vel.Add( amount );
	}
}
}