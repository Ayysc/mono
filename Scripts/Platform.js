class Platform
{
constructor( x,y,gfx )
{
	Platform.prototype.image = gfx.LoadImage( "Images/Square.png" );
	const pos = new Vec2( x,y );
	const size = 64;
	const hitbox = new Rect( pos.x,pos.y,size,size );
	// 
	this.Update=()=>
	{
		hitbox.MoveTo( pos );
	}
	
	this.Draw=( gfx )=>
	{
		// gfx.DrawRect( pos,new Vec2( size,size ),"white" );
		gfx.DrawImage( Platform.prototype.image,pos,new Vec2( size,size ) );
	}
	
	this.GetRect=()=>
	{
		return hitbox;
	}
}
}