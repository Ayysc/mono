class Player
{
constructor( gfx )
{
	const pos = new Vec2( gfx.ScreenWidth / 2,gfx.ScreenHeight / 2 );
	const drawPos = new Vec2( gfx.ScreenWidth / 2,gfx.ScreenHeight / 2 );
	const size = 32;
	// For hit test stuff.
	const offset = size / 4;
	
	let vel = 0.0;
	const speed = 1.6;
	const maxSpeed = speed * 2.5;
	const slowdownFactor = 0.96;
	
	let grav = 0.0;
	const gravAcc = 0.3;
	let jumping = false;
	const jumpPower = 7;
	let canJump = false;
	const maxJumps = 2;
	let nJumpsLeft = 0;
	let holdingSpace = false;
	
	let sliding = false;
	const slideTime = 12;
	let slideTimer = 0.0;
	const slideCooldown = 50;
	let slideCooldownTimer = 0;
	let canSlide = false;
	// 
	this.Update=( kbd )=>
	{
		holdingSpace = kbd.KeyDown( 'W' );
		
		if( kbd.KeyDown( 'A' ) )
		{
			// if( vel > 0.0 ) vel = 0.0;
			vel -= speed;
		}
		else if( kbd.KeyDown( 'D' ) )
		{
			// if( vel < 0.0 ) vel = 0.0;
			vel += speed;
		}
		else if( Math.abs( vel ) < 0.3 ) vel = 0.0;
		
		if( vel > maxSpeed ) vel = maxSpeed;
		if( vel < -maxSpeed ) vel = -maxSpeed;
		
		if( !sliding )
		{
			pos.Add( new Vec2( vel,0.0 ) );
			
			if( ++slideCooldownTimer > slideCooldown )
			{
				slideCooldownTimer = 0;
				canSlide = true;
			}
		}
		else
		{
			pos.Add( new Vec2( vel * 2.0,0.0 ) );
		}
		vel *= slowdownFactor;
		
		grav += gravAcc;
		pos.y += grav;
		
		if( kbd.KeyDown( 'W' ) && canJump )
		{
			jumping = true;
			canJump = false;
		}
		
		if( jumping )
		{
			pos.y -= jumpPower;
		}
		
		if( nJumpsLeft == maxJumps && canSlide )
		{
			if( kbd.KeyDown( 'S' ) )
			{
				sliding = true;
				slideCooldownTimer = 0;
			}
			
			if( sliding && ++slideTimer > slideTime )
			{
				slideTimer = 0;
				sliding = false;
				canSlide = false;
				jumping = true;
				pos.y -= 0.4;
			}
		}
		else
		{
			sliding = false;
			slideTimer = 0;
		}
	}
	
	this.Draw=( gfx )=>
	{
		if( !sliding )
		{
			gfx.DrawRect( pos,new Vec2( size,size ),"red" );
		}
		else
		{
			gfx.DrawRect( pos.GetAdded( new Vec2( 0,size / 2 ) ),
				new Vec2( size,size / 2 ),"red" );
		}
	}
	
	this.CheckHits=( plat )=>
	{
		if( this.HitTestBot( plat.GetRect() ) )
		{
			// this.Move( 0,-1 );
			pos.y = plat.GetRect().y - size;
			this.Land();
		}
		if( this.HitTestTop( plat.GetRect() ) )
		{
			// this.Move( 0,1 );
			pos.y = plat.GetRect().y + plat.GetRect().height;
			this.Fall();
		}
			
		if( this.HitTestLeft( plat.GetRect() ) )
		{
			// this.Move( 1,0 );
			pos.x = plat.GetRect().x + plat.GetRect().width;
			if( holdingSpace ) --nJumpsLeft;
			if( nJumpsLeft > 0 ) this.HalfLand();
		}
			
		if( this.HitTestRight( plat.GetRect() ) )
		{
			// this.Move( -1,0 );
			pos.x = plat.GetRect().x - size;
			if( holdingSpace ) --nJumpsLeft;
			if( nJumpsLeft > 0 ) this.HalfLand();
		}
	}
	
	this.Move=( x,y )=>
	{
		const moveOffset = 0.1;
		pos.Add( new Vec2( x,y ).GetMultiplied( moveOffset ) );
	}
	
	this.Land=()=>
	{
		this.HalfLand();
		nJumpsLeft = maxJumps;
	}
	
	this.HalfLand=()=>
	{
		grav = 0.0;
		jumping = false;
		canJump = true;
	}
	
	this.Fall=()=>
	{
		jumping = false;
		grav = 0.0;
	}
	
	this.HitTestTop=( rect )=>
	{
		if( pos.x + offset < rect.x + rect.width && pos.x + size - offset > rect.x &&
			pos.y < rect.y + rect.height && pos.y + offset > rect.y )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	this.HitTestBot=( rect )=>
	{
		if( pos.x + offset < rect.x + rect.width && pos.x + size - offset > rect.x &&
			pos.y + size - offset < rect.y + rect.height && pos.y + size > rect.y )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	this.HitTestLeft=( rect )=>
	{
		if( pos.x < rect.x + rect.width && pos.x + offset > rect.x &&
			pos.y + offset < rect.y + rect.height && pos.y + size - offset > rect.y )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	this.HitTestRight=( rect )=>
	{
		if( pos.x + size - offset < rect.x + rect.width && pos.x + size > rect.x &&
			pos.y + offset < rect.y + rect.height && pos.y + size - offset > rect.y )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
}