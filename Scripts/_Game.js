"use strict";

(function()
{
const gfx = new Graphics();
const kbd = new Keyboard();
const ms = new Mouse();
const sfx = new AAudio();

const map = new Map();
const pl = new Player( gfx );
const tileSize = 64;
let plats = [];

window.onload = function()
{
	Start();
	const fps = 30;
	setInterval( function()
	{
		if( gfx.CanStart )
		{
			Update();
			Draw();
		}
	},1000 / fps );
}

function Start()
{
	kbd.Start();
	ms.Start( gfx.GetCanvas() );
	gfx.Start();
	// Initialize below!
	for( let y in map.Data )
	{
		for( let x in map.Data[y] )
		{
			if( map.GetPoint( x,y ) == 1 )
			{
				plats.push( new Platform( x * tileSize,y * tileSize,gfx ) );
			}
		}
	}
}

function Update()
{
	// Update below.
	pl.Update( kbd );
	
	for( let i in plats )
	{
		const plat = plats[i];
		
		plat.Update();
		
		pl.CheckHits( plat );
	}
}

function Draw()
{
	gfx.DrawRect( new Vec2( 0,0 ),new Vec2( gfx.ScreenWidth,gfx.ScreenHeight ),"#000" );
	// Draw below.
	for( let i in plats )
	{
		plats[i].Draw( gfx );
	}
	pl.Draw( gfx );
}
})()